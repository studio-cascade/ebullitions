Title: À propos

----

Chapo: <p>Ébullition(s), le cahier illustré de la recherche est une revue bande dessinée, créée par l’Université de Pau et des Pays de l’Adour et éditée par les Presses Universitaires de Pau et des Pays de l'Adour (PUPPA).</p>

----

Body:

Créée dans le cadre de la labellisation Science avec et pour la société (SAPS) de l'UPPA, obtenue en juin 2022, Ébullition(s) est une véritable revue de vulgarisation scientifique, dont les objectifs sont de valoriser la recherche et de la vulgariser afin de rendre les travaux des chercheurs accessibles à tous.
Une revue pour qui, par qui ?

Ébullition(s) cible tous les types de public : chaque article peut être compréhensible aussi bien par un enfant d’école primaire que par un adulte de 77 ans et plus !

Tous les articles sont conjointement réalisés par les chercheurs, les scénaristes et les artistes (dessinateurs, illustrateurs, photographes…). L’intérêt est de proposer un contenu unique issu du dialogue chercheur-auteur(s) permettant de faciliter le lien avec le grand public.
Les thématiques

Les articles vulgarisés abordent toutes les thématiques de recherche de niveau national et international. Les numéros de la revue présentent un contenu éclectique, tant par la forme que par le fond, dont l’objectif est la diffusion de la culture scientifique au sens large. Certains numéros spéciaux pourront également être publiés en fonction de l’actualité. Les articles, rédigés en français, bénéficient d’un référencement par DOI (Digital Object Identifier).
Les formats de la revue

La revue Ébullition(s) se décline en deux versions :

- une version numérique, disponible en open access, gratuite et accessible à tous ;
- une version papier semestrielle et payante, proposant un contenu et une esthétique enrichis.

Contribuer

Vous êtes illustrateur, dessinateur ou scénariste et vous souhaitez participer à la revue Ébullition(s) ?

- Appel à contribution
- Nous contacter: (email: ebullitions@univ-pau.fr text: ebullitions@univ-pau.fr)

----

Legalnotices: Site réalisé par (link: https://julie-blanc.fr/ text: Julie Blanc) et Julia Veljkovic (studio Cascade) et (link: https://adrien-payet.fr text: Adrien Payet)

----

Accessibility:

Lacus nisl tempus suspendisse adipiscing purus nec tristique suspendisse euismod bibendum leo phasellus hendrerit suspendisse quam dolor suspendisse magna tristique ut ut rutrum consectetur adipiscing tempus aliquam nisl felis hendrerit portaest rutrum sem nisi elit id gravida phasellus quis sem rutrum vivamus tortor portaest ipsum accumsan enim hendrerit cursus massa.

Maximus et tortor et molestie consectetur ex eros quisque nec eget pellentesque magna aliquam lacus varius ipsum cursus bibendum quisque dolor phasellus rutrum molestie accumsan sit sed tincidunt orci suspendisse magna euismod purus magna adipiscing lacus portaest magna sem massa quisque proin tempus elementum purus sit nunc purus eros mi.

Varius elit maximus nec lorem ut varius quisque adipiscing erat tortor orci portaest metus scelerisque ac mi vivamus sollicitudin nec ex quisque arcu vel molestie quam metus gravida phasellus quam quis interdum pellentesque quis sed bibendum orci tristique ex lorem tincidunt facilisis leo aliquam gravida placerat sit eget suspendisse facilisis.

----

Privacy: 

----

Text: 

----

Uuid: 9t7YR0peSIESIqqG