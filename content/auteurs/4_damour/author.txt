Title: Damour

----

Type: illustrations

----

Description: Auteur et illustrateur de BD. Il commence sa carrière en 1997 avec la série au long cours *Nash* dont il réalisera dix tomes tout en travaillant en parallèle sur de nombreux *one-shots*. Dessinateur prolifique, réputé pour son trait élégant, il est l'aise dans tous les styles et tous les univers.

----

Affiliation: 

----

Links: 

----

Link: https://praticable.fr

----

Uuid: rdXsra5rbnJ5lixw