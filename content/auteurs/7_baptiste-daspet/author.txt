Title: Baptiste Daspet

----

Type: illustrations

----

Description: Dessinateur de BD. Fraîchement diplômé de l'École Cesan, il co-réalise l'imposant roman graphique *Ciao Vincent* avec Marie-Ange Rousseau sur un scénario de Debora Di Gilio, paru en 2022. Il travaille aujourd'hui sur de nombreux projets de BD courtes pour affiner sa narration.

----

Affiliation: 

----

Links:

- 
  link: https://baptistedaspet.com
  name: 'Site internet de Baptiste Daspet '

----

Link: https://praticable.fr

----

Uuid: sUV1X4VkJyJRSSVb