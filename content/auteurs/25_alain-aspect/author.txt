Title: Alain Aspect

----

Type: texts

----

Description:

Prix Nobel de physique 2022. 
Professeur à l'Institut d'Optique-Université Paris-Saclay. Ses travaux majeurs portent sur les expériences avec des photons intriqués, établissant les violations des inégalités de Bell et ouvrant une voie pionnière vers l'information quantique...

----

Affiliation: Institut d'Optique-Université Paris-Saclay

----

Links:

- 
  link: >
    https://www.lcf.institutoptique.fr/groupes-de-recherche/gaz-quantiques/membres/permanents/alain-aspect
  name: "Alain Aspect (Institut d'Optique-Université Paris-Saclay)"

----

Link: https://praticable.fr

----

Uuid: pVU65z30R5re58tj