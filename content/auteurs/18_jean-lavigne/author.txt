Title: Jean Lavigne

----

Type: illustrations

----

Description: Arpenteur d'espaces, photographe-montagnard dans l'âme, son travail questionne la place de l'homme dans le Vivant et la Terre, confrontée à des désastres.

----

Affiliation: 

----

Links:

- 
  link: http://www.geocime.fr
  name: Site internet de Jean Lavigne

----

Link: https://praticable.fr

----

Uuid: jQ00p1Bqhn1EsZ1N