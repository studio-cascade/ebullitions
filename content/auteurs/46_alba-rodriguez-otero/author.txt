Title: Alba Rodriguez Otero

----

Type: texts

----

Description: Doctorante en chimie analytique à l'IPREM (Institut des sciences Analytiques et de Physico-Chimie pour l'Environnement et les Matériaux), CNRS, UPPA depuis 2021. Sa thèse porte sur l'extraction de silice à partir de balles de riz pour l'adsorption de polluants dans l'eau.

----

Affiliation: Laboratoire IPREM (Institut des sciences Analytiques et de Physico-Chimie pour l'Environnement et les Matériaux)

----

Links:

- 
  link: 'https://www.linkedin.com/in/alba-rodr%C3%ADguez-otero-46456169/?originalSubdomain=fr'
  name: "LinkedIn d'Alba Rodriguez Otero "

----

Link: https://praticable.fr

----

Uuid: IABiRgrePu5DRFsV