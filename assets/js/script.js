function toggleMenu() {
  let body = document.querySelector("body");
  if (body.classList.contains("header-closed")) {
    body.classList.remove("header-closed");
    body.classList.add("header-open");
  } else {
    body.classList.remove("header-open");
    body.classList.add("header-closed");
  }
}

function openBD() {
  document.body.classList.add("bd-open");
  if (!document.body.classList.contains("header-closed")) {
    document.body.classList.remove("header-open");
    document.body.classList.add("header-closed");
  }
}

function closeBD() {
  document.body.classList.remove("bd-open");
}

function setSome(set, callback) {
  let result = false;
  set.forEach((element) => {
    if (callback(element)) {
      result = true;
      return;
    }
  });
  return result;
}

function toggleKeyword(keyword, event) {
  event.preventDefault();
  const isKeywordsPage = window.location.pathname.includes("/mots-cles");
  if (isKeywordsPage) {
    // update Alpine data through a custom window event, listened in keyword.php <main> x-init
    window.dispatchEvent(new Event("toggleKeyword"));
  } else {
    window.location.href =
      window.location.origin + "/mots-cles?keyword=" + keyword;
  }
}

function slugify(str) {
  return String(str)
    .normalize("NFKD")
    .replace(/[\u0300-\u036f]/g, "")
    .trim()
    .toLowerCase()
    .replace(/[^a-z0-9 -]/g, "")
    .replace(/\s+/g, "-")
    .replace(/-+/g, "-")
    .replace("'", "");
}

function closeLightbox() {
  lightbox.classList.remove("show");
}
function enableLightbox(lightbox) {
  document.querySelectorAll("#article-content img").forEach((image) => {
    image.addEventListener("click", () => {
      lightbox.querySelector("img").src = image.src;
      lightbox.classList.add("show");
    });
  });
  const closeLightboxBtn = document.querySelector(".lightbox button.close");
  closeLightboxBtn.addEventListener("click", () => {
    lightbox.classList.remove("show");
  });
  window.addEventListener("keydown", (event) => {
    if (event.key === "Escape") lightbox.classList.remove("show");
  });
}

document.addEventListener("DOMContentLoaded", () => {
  checkScreenSize();

  if (typeof Swiper === "function") {
    const swiper = new Swiper(".swiper", {
      zoom: true,
      direction: "horizontal",
      slidesPerView: 3,
      centeredSlides: true,

      navigation: {
        nextEl: ".button-next",
        prevEl: ".button-prev",
      },

      keyboard: true,
    });

    document.addEventListener("keyup", (event) => {
      if (event.key === "Escape") {
        closeBD();
      }
    });

    document.querySelectorAll(".swiper-zoom-container").forEach((img) => {
      img.addEventListener("click", (event) => {
        swiper.zoom.toggle(event);
      });
    });

    const lightbox = document.querySelector(".lightbox");
    if (lightbox) {
      enableLightbox(lightbox);
    }
  }
});

function checkScreenSize() {
  if (
    document.body.dataset.layout !== "home" &&
    document.body.dataset.layout !== "group-list"
  ) {
    if (window.innerWidth < 1200) {
      document.body.classList.add("header-closed");
    }
  } else {
    if (window.innerWidth <= 1180 && window.innerWidth <= 1024) {
      document.body.classList.add("header-closed");
    }
  }
}

window.addEventListener("resize", () => {
  if (window.innerWidth <= 1024) {
    if (!document.body.classList.contains("header-closed")) {
      document.body.classList.add("header-closed");
      document.body.classList.add("header-closed-js");
    }
  } else {
    if (document.body.classList.contains("header-closed-js")) {
      document.body.classList.remove("header-closed");
      document.body.classList.remove("header-closed-js");
    }
  }
});
