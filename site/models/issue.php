<?php

class IssuePage extends Page {
    public function realIndex() {
      if ($this->status() == 'listed') {
        return 'Numéro ' . $this->num() - 1;
      } elseif ($this->status() == 'unlisted') {
        return 'Numéro hors-série';
      } else {
        return 'Brouillon';
      }
    }
}