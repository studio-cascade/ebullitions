<?php

class ArticlePage extends Page {
    public function thematic() {
        $thematics             = page('dossiers-thematiques')->children();
        $correspondingThematic = false;

        foreach ($thematics as $thematic) {
            foreach ($thematic->articles()->toPages() as $article) {           
                if ($article->uri() == $this->uri()) {
                    $correspondingThematic = $thematic;
                }
            }
        }

        return $correspondingThematic;
    }
    
    public function issue() {
        $issues             = page('numeros')->children();
        $correspondingIssue = false;

        foreach ($issues as $issue) {
            foreach ($issue->articles()->toPages() as $article) {           
                if ($article->uri() == $this->uri()) {
                    $correspondingIssue = $issue;
                }
            }
        }

        return $correspondingIssue;
    }

    public function location() {
        if (!$this->issue() && !$this->thematic()) return 'Article non classé.';
        if ($this->issue() && $this->thematic()) return 'Dans le n° [' . $this->issue()->title() . '](' . $this->issue()->panel()->url() . ') et le dossier [' . $this->thematic()->title() . '](' . $this->thematic()->panel()->url() . ').';
        if ($this->issue()) return 'Dans le n° [' . $this->issue()->title() . '](' . $this->issue()->panel()->url() . ').';
        if ($this->thematic()) return 'Dans le dossier ' . $this->thematic()->title() . '(' . $this->thematic()->panel()->url() . ').';
    }

    public function shortLocation() {
        if (!$this->issue() && !$this->thematic()) return 'Non classé';
        if ($this->issue() && $this->thematic()) return $this->issue()->title() . ' et ' . $this->thematic()->title();
        if ($this->issue()) return $this->issue()->title();
        if ($this->thematic()) return $this->thematic()->title();
    }
}