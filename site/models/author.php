<?php

class AuthorPage extends Page {
    public function articles() {
        $articles     = site()->index()->filterBy('template', 'article');
        $publications = $articles->filter(function($article) {
            $authors = $article->textAuthors()->toPages()->add($article->illustrationsAuthors()->toPages());
            foreach ($authors as $author) {
                if ($author->uri() === $this->uri()) {
                    return true;
                }
            }
        });

        return $publications;
    }
    
    public function thematics() {
        $allThematics     = page('dossiers-thematiques')->children();
        $thematics = $allThematics->filter(function($thematic) {
            $authors = $thematic->directors()->toPages();
            foreach ($authors as $author) {
                if ($author->uri() === $this->uri()) {
                    return true;
                }
            }
        });

        return $thematics;
    }
}