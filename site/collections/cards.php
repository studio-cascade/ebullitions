<?php

return function ($site) {
    $articles  = $site->index()->filterBy('template', 'article')->limit(9);
    $thematics = page('dossiers-thematiques')->children()->limit(1);

    return $articles->merge($thematics);
};