<?php

return function() {
    $nav = new Pages();
    $nav->add(page('home'));
    $nav->add(page('numeros'));
    $nav->add(page('articles'));
    $nav->add(page('actualites'));
    
    if (page('dossiers-thematiques')->hasChildren()) {
        $nav->add(page('dossiers-thematiques'));
    }
    
    $nav->add(page('auteurs'));
    $nav->add(page('appel-a-contributions'));
    $nav->add(page('a-propos'));

    return $nav;
};