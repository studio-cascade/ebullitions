<?php

return function() {
    $groups = array(
        'A' => new Pages(),
        'B' => new Pages(),
        'C' => new Pages(),
        'D' => new Pages(),
        'E' => new Pages(),
        'F' => new Pages(),
        'G' => new Pages(),
        'H' => new Pages(),
        'I' => new Pages(),
        'J' => new Pages(),
        'K' => new Pages(),
        'L' => new Pages(),
        'M' => new Pages(),
        'N' => new Pages(),
        'O' => new Pages(),
        'P' => new Pages(),
        'Q' => new Pages(),
        'R' => new Pages(),
        'S' => new Pages(),
        'T' => new Pages(),
        'U' => new Pages(),
        'V' => new Pages(),
        'W' => new Pages(),
        'X' => new Pages(),
        'Y' => new Pages(),
        'Z' => new Pages()
    );

    $authors = page('auteurs')->children();

    foreach ($authors as $author) {
        $name        = explode(' ', $author->title()->value());
        $lastName    = $name[count($name) - 1];
        $firstLetter = strtoupper(substr($lastName, 0, 1));
        $groups[$firstLetter]->add($author);
    }
    

    return $groups;
};