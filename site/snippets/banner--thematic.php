<?php
    $thematic = $thematic ?? $page;
?>

<section class="banner">
    <p class="category">Dossier thématique</p>
    <div class="group-title">
    <p class="num">Dossier</p>
   
        <h1>
            <a href="<?= $thematic->url() ?>"><?= $thematic->title() ?></a>
        </h1>
        
        <div class="authors">
            <p class="texts">Textes réunis par</p>
            <p><?= $thematic->directors()->toPages()->listAll() ?> </p>
        </div>
    </div>
   
    <div class="infos">
            <p class="date"><?= $thematic->date() ?></p>
            <p class="articles">
                <?= $thematic->articles()->toPages()->count() ?>
                article<?= e($thematic->articles()->toPages()->count() > 1, 's') ?>
            </p>
        </div>

    <?php if ($page->template() == 'thematic'): ?>
        <figure><img src="<?= $page->cover()->toFile()->url() ?>" alt="<?= $page->cover()->toFile()->alt() ?>"/></figure>
    <?php else: ?>
            <figure><img src="<?= $thematic->cover()->toFile()->url() ?>" alt="<?= $thematic->cover()->toFile()->alt() ?>"/></figure>
    <?php endif ?>

  

    <div class="group-button">
        <button class="button-full" tabindex="-1">
            <a href="<?= $thematic->url() ?>">Lire en ligne</a>
        </button>
    </div>
</section>