<section class="banner">
    <h1>Auteur·ice·s</h1>

    <?php foreach($kirby->collection('ordered-authors') as $letter => $authors): ?>

    <?php if ($authors->count() > 0): ?>

    <h2><?= $letter ?></h2>

    <?php foreach($authors as $author): ?>
    <p><a
            href="#<?= $author->slug() ?>"><?= $author->title() ?></a>
    </p>
    <?php endforeach ?>

    <?php endif ?>

    <?php endforeach ?>
</section>