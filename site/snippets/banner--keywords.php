<section class="banner banner-keywords">
  <h1>Mots-clés</h1>

  <?php foreach($site->keywords()->split() as $keyword): ?>
    <button class="keyword"><?= $keyword ?><span class="nbr" x-text="itemsPerKeyword()"></span></button>
  <?php endforeach ?>
</section>