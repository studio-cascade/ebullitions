<section class="banner">
    <div class="group-title">
    <?php if ($page->status() == 'unlisted'): ?>
        <p class="num">
            <span>Hors-série</span>
        </p>
    <?php endif ?>
     
        
    </div>

    <figure><img src="<?= $page->cover()->toFile()->url() ?>" alt="<?= $page->cover()->toFile()->alt() ?>">
    </figure>
    <div class="group-button">
        <button class="button-full" tabindex="-1"><a href="<?= e($page->buyLink()->isNotEmpty() == true, $page->buyLink(), 'https://www.canalbd.net/bachi-bouzouk/editeurs/puppa-3299/') ?>" target="_blank">Acheter la version papier</a></button>
    </div>

</section>