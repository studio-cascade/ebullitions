<article class="card-author" id="<?= $author->slug() ?>">
    <h1><?= $author->title() ?></h1>
    <?php if ($author->affiliation()->isNotEmpty()): ?>
    <p class="affiliation"><?= $author->affiliation() ?></p>
    <?php endif ?>
    <p class="bio"><?= $author->description()->kt() ?></p>
    <?php if ($author->links()->isNotEmpty()): ?>
    <ul class="links">
        <?php foreach($author->links()->toStructure() as $item): ?>
        <li>
            <a href="<?= $item->link()->toUrl() ?>"<?= e(!str_contains($item->link(), $site->url()), ' target="_blank"') ?>>
                <?= e($item->name()->isNotEmpty(), $item->name(), $item->link()) ?>
            </a>
        </li>
        <?php endforeach ?>
    </ul>
    <?php endif ?>

    <?php if ($author->articles()->count() > 0): ?>
    <div class="list-publis">
        <p>Article(s)</p>
        <ul>
            <?php foreach($author->articles() as $article): ?>
            <li data-type="article">
                <a href="<?= $article->url() ?>">
                    <p class="title"><?= $article->title() ?>
                        <?php
                            $isTextAuthor = $article->textAuthors()->toPages()->has($author->uri());
                        ?>
                        <span><?= e($isTextAuthor, 'textes', 'illustrations') ?></span>
                    </p>
                </a>
            </li>
            <?php endforeach ?>
        </ul>
    </div>
    <?php endif ?>

    <?php if ($author->thematics()->count() > 0): ?>
    <div class="list-publis">
        <p>Dossier(s) thématique(s)</p>
        <ul>
            <?php foreach($author->thematics() as $thematic): ?>
            <li data-type="dossier">
                <a href="<?= $thematic->url() ?>">
                    <p class="title"><?= $thematic->title() ?> <span><?= $thematic->articles()->toPages()->count() ?>&nbsp;articles</span></p>
                </a>
            </li>
            <?php endforeach ?>
        </ul>
    </div>
    <?php endif ?>


</article>