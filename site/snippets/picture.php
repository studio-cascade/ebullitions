<?php if ($file):

$sizes = '(min-width: 1200px) 25vw, (min-width: 900px) 33vw, (min-width: 600px) 50vw, 100vw';
$alt = $file->alt();
$crop = $crop ?? false;

$webPSrcset = $file->srcset('webp');
$srcset = $file->srcset();
$src = $file->url();
$width = $file->resize(1800)->width();
$height = $file->resize(1800)->height();

?>


<picture>
    
    <source srcset="<?= $webPSrcset ?>"
        sizes="<?= $sizes ?>" type="image/webp">
    <img 
        src="<?= $src ?>"
        srcset="<?= $srcset ?>"
        sizes="<?= $sizes ?>"
        width="<?= $width ?>"
        height="<?= $height ?>"
        alt="<?= $alt?>"
        loading="lazy"
    >
    <?= svg('assets/images/loader.svg') ?>
</picture>

<?php endif ?>