<?php 

    $layout   = $layout ?? '';
    $type     = $type ?? '';
    $pageType = $pageType ?? '';

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= e($page->isHomePage() != true, $page->title() . ' - ') . $site->title() ?>
    </title>
    <link rel="stylesheet" type="text/css" href="<?= url('assets/css/style.css?version-cache-prevent<?= rand(0, 1000)?>') ?>">
    <link rel="stylesheet" type="text/css" href="<?= url('assets/fonts/syne/stylesheet.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= url('assets/fonts/abordage/stylesheet.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= url('media/plugins/adrienpayet/edit-button/style.css') ?>">

    <script src="<?= url('assets/js/script.js') ?>">
    </script>

    <?php snippet('front-comments') ?>

    <?= e($page->template() == 'article', js('https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js', 'defer')) ?>
    <?= e($page->template() == 'article', css('https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css')) ?>
    
    <?= e($page->template() == 'articles' || $page->template() == 'keywords', js('https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js', 'defer')) ?>

    <?php snippet('seo/head'); ?>
</head>

<body data-layout="<?= $layout ?>"
    data-type="<?= $type ?>"
    data-page-type="<?= $pageType ?>">

    <?php snippet('symbols') ?>