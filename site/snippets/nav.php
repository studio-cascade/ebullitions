

<header id="header">
  <button id="button-header" onclick="toggleMenu()" title="ouvrir / fermer le menu">
    <?= svg('assets/images/icons/nav.svg') ?>
  </button>


  <div class="content">

    <div id="baseline">
      <a href="<?= $site->url() ?>">
        <div id="logo">
          <?= svg('assets/images/logo.svg') ?>
        </div>
      </a>
    </div>

    <nav>
      <ul>
        <?php foreach ($kirby->collection('nav') as $item): ?>
        <li <?php
          if ($page->isHomePage() == 'true') {
            e($page->url() == $item->url(), 'class="selected"');
          } else {
            e($item->isHomePage() != 'true' && str_contains($page->url(), $item->url()), 'class="selected"');
          }
        ?>>
    
          <a href="<?= $item->url() ?>"><?= $item->title() ?></a>
        </li>
        <?php endforeach ?>

      </ul>
    </nav>
  </div>
  </div>
  <ul id="highlight">
    <?php foreach($site->links()->toStructure() as $item): ?>
      <li><a href="<?= $item->link() ?>" target="_blank"><?= $item->text() ?></a></li>    
    <?php endforeach ?>
  </ul>
</header>

