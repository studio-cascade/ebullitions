<?php 
    // snippet('edit-button', [
    //     'text' => 'éditer la page', 
    //     'home' => 'site'
    // ]) 
?>
<footer id="main-footer">
    <p><a href="/a-propos#mentions-legales">Mentions légales</a> - <a href="/a-propos#accessibilite">Accessibilité</a> - <a href="/a-propos#donnees-personnelles">Données personnelles</a></p>
</footer>
<?php snippet('seo/schemas'); ?>
</body>

</html>