<section id="card--article-cover" data-type="article" class="card-block">
        <p class="type">Article</p>
        <div class="infos">
            <p class="date">
                <?= $article->created()->toDate('d/m/Y') ?>
            </p>
        </div>
        <h1><a href="<?= $article->url() ?>"><?= $article->title() ?></a>
        </h1>
        <p class="description">
            <?php if ($article->textAuthors()->isNotEmpty()): ?>
                <?php 
                    $index = 1;
                    $textAuthors = $article->textAuthors()->toPages();
                    foreach($textAuthors as $textAuthor): ?>
                    <a href="/auteurs#<?= $textAuthor->slug() ?>"><?= $textAuthor->title() ?></a><?= e($index < $textAuthors->count(), ', ') ?>
                    <?php $index++ ?>
                <?php endforeach ?>
                (texte)
            <?php endif ?>
            
            <?php if ($article->illustrationsAuthors()->isNotEmpty()): ?>
                ; 
                <?php
                    $index = 1;
                    $illustrationsAuthors = $article->illustrationsAuthors()->toPages();
                    foreach($illustrationsAuthors as $illustrationsAuthor): ?>
                    <a href="/auteurs#<?= $illustrationsAuthor->slug() ?>"><?= $illustrationsAuthor->title() ?></a><?= e($index < $illustrationsAuthors->count(), ', ') ?>
                    <?php $index++ ?>
                <?php endforeach ?>
                (illustrations)
            <?php endif ?>
        </p>
        <figure>
            <?php
                $file = $article->cover()->isNotEmpty()
                    ? $article->cover()->toFile()->thumb()
                    : ($article->bd()->isEmpty()
                        ? $article->files()->first()->thumb()
                        : $article->bd()->toFiles()->first()->thumb());
            ?>
            <a href="<?= $article->url() ?>" title="Voir l'article">
                <img src="<?= $file->url() ?>" alt="<?= $file->alt() ?>" <?= e($article->bd()->isEmpty(), 'class="no-bd"') ?>>
            </a>
        </figure>

        <ul class="keywords">
            <?php foreach($article->keywords()->split() as $keyword): ?>
            <li>
                <a href="/mots-cles?keyword=<?= $keyword ?>">
                    <?= $keyword ?>
                </a>
            </li>
            <?php endforeach ?>
        </ul>
        <div class="group-button">
            <button class="button-full" tabindex="-1">
                <a href="<?= $article->url() ?>">
                    Lire en ligne
                </a>
            </button>
        </div>
    </section>