<section id="section-actus" data-type="actu" class="card-actu card-block">
    <p class="type">Actualité</p>
    <div class="infos">
        <p class="date"><?= $news->category() ?></p>
        <p class="articles"><?= $news->created()->toDate('d/m/Y') ?></p>
    </div>
    <h1><a href="<?= $news->url() ?>"><?= $news->title() ?></a></h1>
    <h2><?= $news->subtitle() ?></h2>
    <p class="chapo">
        <?= $news->chapo() ?>
    </p>

    <figure>
        <?php snippet('picture', ['file' => $news->cover()->toFile()]) ?>
    </figure>
    <div class="group-button">
        <button class="button-outline" tabindex="-1"><a href="<?= page('actualites')->url() ?>">Toutes les actualités</a></button>
        <button class="button-full" tabindex="-1"><a href="<?= $news->url() ?>">Voir plus</a></button>
    </div>
</section>