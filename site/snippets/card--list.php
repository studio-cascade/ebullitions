<?php
$type = $type ?? 'article';
?>

<article class="card" data-type="<?= $type ?>">
    <div class="content">
        <a href="<?= $card->url() ?>">

            <h1><?= $card->title() ?></h1>
            
            <p class="description">
                <?php if ($type == 'article'): ?>
                <?= $card->textAuthors()->toPages()->listAll() ?>
                (texte);
                <?= $card->illustrationsAuthors()->toPages()->listAll() ?>
                (illustrations)
                <?php else: ?>
                    textes réunis par <?php 
                        $directors = $card->directors()->toPages();
                        if ($directors->count() < 2) {
                            echo $directors->first()->title();
                        } else {
                            echo $directors->first()->title() . ' et ' . $directors->nth(1)->title();
                        }
                    ?>
                <?php endif ?>
            </p>

            <div class="details">
                <p class="date">
                    <?= $card->created()->toDate('d/m/Y') ?>
                </p>
            </div>

        </a>
    </div>

    <?php if ($card->keywords()->isNotEmpty()): ?>
    <ul class="keywords">
        <?php foreach($card->keywords()->split() as $keyword): ?>
        <li :class="selectedKeywords.has('<?= $keyword ?>') ? 'selected': ''" onclick="toggleKeyword('<?= $keyword ?>', event)">
            <a href="/mots-cles?keyword=<?= $keyword ?>"><?= $keyword ?></a>
        </li>
        <?php endforeach ?>
    </ul>
    <?php endif ?>
</article>