<article class="card-block"
    data-type="<?= e($card->template() == 'article', 'article', 'dossier') ?>">
    <p class="type">
        <?= e($card->template() == 'article', 'Article', 'Dossier') ?>
    </p>
    <div class="infos">
        <p class="date">
            <?= $card->created()->toDate('d/m/Y') ?>
        </p>
        <?php if ($card->template() == 'thematic'): ?>
        <p class="articles"><?= $card->articles()->toPages()->count() ?> articles</p>
        <?php endif ?>
    </div>

    <h1><a
            href="<?= $card->url() ?>"><?= $card->title() ?></a>
    </h1>
    <p class="description">
        <?php if ($card->template() == 'article'): ?>
            <?php if ($card->textAuthors()->isNotEmpty()): ?>
                <?php 
                    $index = 1;
                    $textAuthors = $card->textAuthors()->toPages();
                    foreach($textAuthors as $textAuthor): ?>
                    <a href="/auteurs#<?= $textAuthor->slug() ?>"><?= $textAuthor->title() ?></a><?= e($index < $textAuthors->count(), ', ') ?>
                    <?php $index++ ?>
                <?php endforeach ?>
                (texte)
            <?php endif ?>
            
            <?php if ($card->illustrationsAuthors()->isNotEmpty()): ?>
                ; 
                <?php
                    $index = 1;
                    $illustrationsAuthors = $card->illustrationsAuthors()->toPages();
                    foreach($illustrationsAuthors as $illustrationsAuthor): ?>
                    <a href="/auteurs#<?= $illustrationsAuthor->slug() ?>"><?= $illustrationsAuthor->title() ?></a><?= e($index < $illustrationsAuthors->count(), ', ') ?>
                    <?php $index++ ?>
                <?php endforeach ?>
                (illustrations)
            <?php endif ?>

        <?php elseif ($card->directors()->isNotEmpty()): ?>
            <span class="texts">Textes réunis par</span>
            <?php 
                $index = 1;
                $directors = $card->directors()->toPages();
                foreach($directors as $director): ?>
                <a href="/auteurs#<?= $director->slug() ?>"><?= $director->title() ?></a><?= e($index < $directors->count(), ', ') ?>
                <?php $index++ ?>
            <?php endforeach ?>
        <?php endif ?>
    </p>

    <?php if ($card->keywords()->isNotEmpty()): ?>
    <ul class="keywords">
        <?php foreach($card->keywords()->split() as $keyword): ?>
        <li>
            <a href="/mots-cles?keyword=<?=  $keyword?>">
                <?= $keyword ?>
            </a>
        </li>
        <?php endforeach ?>
    </ul>
    <?php endif ?>

    <div class="group-button">
        <button class="button-outline" tabindex="-1"><a href="<?= e($card->template() == 'article', '/articles', '/dossiers-thematiques') ?>"><?= e($card->template() == 'article', 'Tous les articles', 'Tous les dossiers') ?></a></button>
        <button class="button-full" tabindex="-1">
            <a href="<?= $card->url() ?>">Lire en ligne</a>
        </button>
    </div>
</article>