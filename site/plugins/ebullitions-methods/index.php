<?php

Kirby::plugin('adrienpayet/ebullitions-methods', [
    'pagesMethods' => [
        
        // Returns a string listing the names of a collection of authors
        'listAll' => function ($mode = 'link', $separator = ',') {
            $titles = array_map(function($page) use($mode) {
                if ($mode === 'link') {
                    return '<a href="/auteurs#' . Str::slug($page['content']['title']) . '">' . $page['content']['title'] . '</a>';
                } else {
                    return Str::replace(Str::slug($page['content']['title']) . $page['content']['title'], '\'', '');
                }
            }, $this->toArray());

            echo implode($separator . ' ', $titles);
        },
        
        // Sorts a collection of authors alphabetically by their last names
        'sortByLastNames' => function() {
            return $this->sort(function($author) {
                $fullName = $author->title()->value();
                $names    = explode(' ', $fullName);
                $lastName = $names[count($names) - 1];
    
                return $lastName;
            }, SORT_ASC);
        }
    ]
]);
