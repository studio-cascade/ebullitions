<?php

return [
    'debug'  => true,
    'thumbs' => [
        'quality' => 80,
        'presets' => [
            'default' => [
                'width' => 1024, 'format' => 'webp'
            ],
            'full' => 2048, 'format' => 'webp'
        ],
        'srcsets' => [
            'default' => [
                '200w'  => ['width' => 400],
                '400w'  => ['width' => 800],
                '800w'  => ['width' => 1600],
                '1024w' => ['width' => 2048],
                '1440w' => ['width' => 2880],
                '2048w' => ['width' => 4096]
            ],
            'webp' => [
                '200w'  => ['width' => 400, 'format' => 'webp'],
                '400w'  => ['width' => 800, 'format' => 'webp'],
                '800w'  => ['width' => 1600, 'format' => 'webp'],
                '1024w' => ['width' => 2048, 'format' => 'webp'],
                '1440w' => ['width' => 2880, 'format' => 'webp'],
                '2048w' => ['width' => 4096, 'format' => 'webp']
            ]
        ],
    ],
    'panel' => [
        'css' => 'assets/css/custom-panel.css',
        'menu' => require __DIR__ . '/menu.php'
    ],

    'tobimori.seo.canonicalBase' => 'https://revue-ebullitions.fr',
    'tobimori.seo.lang' => 'fr_FR'
];
