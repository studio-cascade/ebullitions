<?php

return [
  'site' => [
    'label' => 'Publication',
    'icon' => 'pen',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'site');
    }
  ],
  'home' => [
    'label' => 'Accueil',
    'icon' => 'home',
    'link' => 'pages/home',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'home');
    }
  ],
  'news' => [
    'label' => 'Actus',
    'icon' => 'bell',
    'link' => 'pages/actualites',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/actualites');
    }
  ],
  'call-for-papers' => [
    'label' => 'Contributions',
    'icon' => 'megaphone',
    'link' => 'pages/appel-a-contributions',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/appel-a-contributions');
    }
  ],
  'about' => [
    'label' => 'À propos',
    'icon' => 'question',
    'link' => 'pages/a-propos',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/a-propos');
    }
  ],
  '-',
  '-',
  'users',
  'system'
];
