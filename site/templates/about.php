<?php snippet('header', [
    'layout'   => 'no-banner',
    'type'     => 'about',
    'pageType' => 'static'
]) ?>
<?php snippet('nav') ?>

<main>

    <section id="main-content">
        <header class="header-content">
            <h1><?= $page->title() ?></h1>
        </header>
        <div class="chapo">
            <?= $page->chapo() ?>
        </div>
        <?= $page->body()->kt() ?>
        <h2 id="mentions-legales">Mentions légales</h2>
        <?= $page->legalNotices()->kt() ?>
        <h2 id="accessibilite">Accessibilité</h2>
        <?= $page->accessibility()->kt() ?>
        <h2 id="donnees-personnelles">Données personnelles</h2>
        <?= $page->privacy()->kt() ?>
    </section>

</main>
<?php snippet('footer') ?>