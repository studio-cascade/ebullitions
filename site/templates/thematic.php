<?php snippet('header', [
    'layout'   => 'group',
    'type'     => 'dossier',
    'pageType' => 'page-dossier'
]) ?>


<?php snippet('nav') ?>

<main>
<section class="banner">
    <p class="num">Dossier</p>
    <?php if ($page->template() == 'thematic'): ?>
        <figure><img src="<?= $page->cover()->toFile()->url() ?>" alt="<?= $page->cover()->toFile()->alt() ?>"/></figure>
    <?php else: ?>
            <figure><img src="<?= $thematic->cover()->toFile()->url() ?>" alt="<?= $thematic->cover()->toFile()->alt() ?>"/></figure>
    <?php endif ?>
    <!-- <p>« <?= $page->title() ?> »</p> -->

    <div class="group-title">
        <div class="authors">Textes réunis par
            <p><?= $page->directors()->toPages()->listAll() ?></p>
        </div>
    </div>
</section>


    <section id="main-content">
        <header class="header-content">
            <p class="category">Dossier thématique</p>
            <p class="infos"><span class="date"><?= $page->date() ?></span> <span>3 articles</span> </p>
            <h1><?= $page->title() ?></h1>

            <p class="authors">Textes réunis par <?= $page->directors()->toPages()->listAll() ?> </p>
        </div>
        </header>



        <div class="keywords">
            <ul>
                <?php foreach($page->keywords()->split() as $keyword): ?>
                    <li>
                        <a href="/mots-cles?keyword=<?= $keyword ?>">
                        <?= $keyword ?>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>

        <section id="edito">

            <input type="checkbox" id="toggle-edito" name="toggle-edito" checked>

            <div id="text-edito">

                <div class="content">

                    <?= $page->edito() ?>


                    <div class="authors-edito section-infos">
                        <?php foreach($page->directors()->toPages() as $author): ?>
                        <div class="author">
                            <p><a
                                    href="<?= $author->url() ?>"><?= $author->title() ?></a>
                            </p>
                            <p class="author-description">
                                <?= $author->description() ?>
                            </p>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>

                <div class="group-button">
                    <label class="button-full" id="read-edito" for="toggle-edito">Lire l’édito</label>
                    <label class="button-full" id="close-edito" for="toggle-edito">Fermer l’édito</label>
                </div>

            </div>
        </section>

        <section id="list-articles">
            <?php foreach($page->articles()->toPages() as $card): ?>
            <?php snippet('card', ['card' => $card]) ?>
            <?php endforeach ?>
        </section>
    </section>
</main>

<?php snippet('footer') ?>