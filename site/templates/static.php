<?php snippet('header', [
    'layout'   => 'no-banner',
    'type'     => 'site',
    'pageType' => 'static'
]) ?>
<?php snippet('nav') ?>

<main>

    <section id="main-content">
        <header class="header-content">
            <h1><?= $page->title() ?></h1>
        </header>
        <div class="chapo">
            <?= $page->chapo() ?>
        </div>
        <?= $page->body()->kt() ?>
    </section>

</main>
<?php snippet('footer') ?>