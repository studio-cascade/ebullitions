<?php snippet('header', [
    'layout'   => 'banner',
    'type'     => 'site',
    'pageType' => 'authors'
]) ?>


<?php snippet('nav') ?>

<main>

    <?php snippet('banner--authors') ?>

    <section id="main-content">
    <header class="header-content">
                <h1>Auteurs.autrices</h1>
        </header>

        <?php foreach(page('auteurs')->children()->sortByLastNames() as $author): ?>

        <?php snippet('card--author', ['author' => $author]) ?>

        <?php endforeach ?>
    </section>
</main>

<?php snippet('footer') ?>