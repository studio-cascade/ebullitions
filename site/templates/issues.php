<?php snippet('header', [
    'layout'   => 'group-list',
    'type'     => 'numero',
    'pageType' => 'list-numeros'
]) ?>

<?php snippet('nav') ?>

<main>
    <!-- <h1 class="title-phone">Numéros</h1> -->
    <div class="container-banners"
        style="--group-nbr: <?= $page->children()->count() ?>;">
      

        <?php foreach($page->children()->flip() as $issue): ?>
        <div class="banner">
                <div class="group-title">
                <p class="num">
                <?php if ($issue->status() == 'listed'): ?>
                    <span>Numéro <?= $issue->num() - 1 ?></span>
                <?php else: ?>
                    <span>Hors-série</span>
                <?php endif ?>
            </p>
            <h1>
                <a href="<?= $issue->url() ?>">
                    <?= $issue->title() ?> 
                        <?php if ($issue->subtitle()->isNotEmpty()): ?>
                            <span class="sub-title"><?= $issue->subtitle() ?></span> 
                        <?php endif ?>
                    </a> 
                </h1>
                    
                    
                </div>

                <div class="infos">
                        <p class="articles">
                            <?= $issue->articles()->toPages()->count() ?>
                            article<?= e($issue->articles()->toPages()->count() > 1, 's') ?>
                        </p>
                    </div>
                
                <figure><img
                        src="<?= $issue->cover()->toFile()->thumb()->url() ?>" alt="<?= $issue->cover()->toFile()->thumb()->alt() ?>">
                </figure>
                <div class="group-button">
                    <button class="button-outline" tabindex="-1"><a
                            href="<?= $issue->url() ?>">Lire en
                            ligne</a></button>
                    <button class="button-full" tabindex="-1"><a href="<?= e($issue->buyLink()->isNotEmpty() == true, $issue->buyLink(), 'https://www.canalbd.net/bachi-bouzouk/editeurs/puppa-3299/') ?>" target="_blank">Acheter</a></button>
                </div>
        </div>

        <?php endforeach ?>
        

    </div>
</main>
<?php snippet('footer') ?>