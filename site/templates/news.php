<?php snippet('header', [
    'layout'   => 'full',
    'type'     => 'site',
    'pageType' => 'actualites'
]) ?>

<?php snippet('nav') ?>
<main>

    <section id="main-content">

        <header class="header-content">
            <h1>Actualités</h1>
        </header>

        <?php foreach($page->children() as $news): ?>
        <?php snippet('card--news', ['news' => $news]) ?>
        <?php endforeach ?>
    </section>

</main>

<?php snippet('footer') ?>