<?php snippet('header', [
    'layout'    => $page->bd()->isEmpty() ? 'no-banner' : 'article',
    'type'      => 'article',
    'page-type' => 'page-article'
]) ?>


<?php snippet('nav') ?>

<main>
    <div class="lightbox">
        <button class="close">fermer</button>
        <figure>
            <img src="" alt="">
        </figure>
    </div>

    <section id="main-content">

    <div class="banner-phone">
        <a class="banner-link-phone" href="#banner-bd">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 500 500"><g><g id="sv-voir-bd"><path d="M152.2,393.4c-2.5,0-4.9-.9-6.9-2.5-3-2.6-4.4-6.6-3.4-10.5h0c1.3-5.6,2.6-11,3.9-16.2,3-12.3,5.8-23.9,8.5-35.6,2.1-9,2.4-13.8,1.2-16.7-1.2-2.9-5.1-6.2-13-11-12.8-7.8-23.4-16.6-32.4-27-28.8-33.4-27.2-83.2,3.6-115.9,23.5-24.9,53.8-40.4,92.7-47.3,30.7-5.5,59.5-5.8,85.5-.9,28.8,5.4,55.3,17.3,78.9,35.4,25.5,19.5,39.7,45.1,39.9,71.9.2,27-13.7,52.9-39.2,72.9-31.5,24.8-70.2,37.8-114.8,38.7-13.9.3-21.4,3.9-28.6,13.7-18.8,25.8-45.7,39.5-71.9,50.4-1.3.5-2.7.8-4.1.8ZM161.4,385.1h0s0,0,0,0ZM253.4,126.2c-13.8,0-28.3,1.4-43.5,4.1-34.5,6.2-61.2,19.7-81.6,41.4-23.8,25.2-25,63.5-3,89.1,7.6,8.8,16.6,16.3,27.6,23,22.3,13.5,26.8,24.1,20.9,49.3-2.6,11-5.2,21.9-8,33.3,17.5-8.6,34.2-19.5,46.1-35.9,10.9-14.9,24.1-21.5,44.3-21.9,40.8-.8,74.4-12.1,102.9-34.4,20.5-16.1,31.7-36.4,31.5-57.1-.1-20.5-11.5-40.5-32-56.2-30.4-23.2-65.1-34.7-105.3-34.7Z"/><path d="M500,499.7H0V0h500v499.7ZM19.3,480.4h461.5V19.2H19.3v461.2Z"/></g></g></svg>
            <p>Lire la bande dessinée</p>
        </a>
    </div>

        <header class="header-content" id="article-header">
            <p class="category">Article</p>
            <h1><?= $page->title() ?></h1>
            <div class="contexte">
                <?php if ($page->issue()): ?>
                    <a href="<?= $page->issue()->url() ?>">Numéro <?= $page->issue()->num() - 1 ?></a>
                <?php endif ?>
                <?php if ($page->thematic()): ?>
                    <a href="<?= $page->thematic()->url() ?>">Dossier « <?= $page->thematic()->title() ?> »</a>
                <?php endif ?>
            </div>
        </header>

        <section id="article-infos" class="section-infos">

            <div id="authors" class="group">
                <h2>Auteur·ice(s)</h2>
                <div id="authors-texte">
                    <h3>Textes</h3>
                    <?php foreach($page->textAuthors()->toPages() as $textAuthor): ?>
                    <p class="author-name"><a
                            href="/auteurs#<?= $textAuthor->slug() ?>"><?= $textAuthor->title() ?></a>
                    </p>
                    <p class="author-description">
                        <?= $textAuthor->description()->kti() ?>
                    </p>
                    <?php endforeach ?>

                </div>
                <div id="authors-illustration">
                    <h3>Illustrations</h3>
                    <?php foreach($page->illustrationsAuthors()->toPages() as $illustrationsAuthor): ?>
                    <p class="author-name"><a
                            href="/auteurs#<?= $illustrationsAuthor->slug() ?>"><?= $illustrationsAuthor->title() ?></a>
                    </p>
                    <p class="author-description">
                        <?= $illustrationsAuthor->description()->kti() ?>
                    </p>
                    <?php endforeach ?>
                </div>
                <?php if ($page->podcast()->isNotEmpty()): ?>
                    <div id="podcast">
                        <h2>Podcast</h2>
                        <p>Retrouvez la version podcast de cet article au lien suivant : <a href="<?= $page->podcast() ?>" target="_blank">[lien podcast]</a></p>  
                    </div>
                <?php endif ?>
            </div>

            <div id="infos" class="group">
                <h2>Informations</h2>

                <div class="keywords">
                    <h3>Mots-clés</h3>
                    <ul>
                        <?php foreach($page->keywords()->split() as $keyword): ?>
                        <li>
                            <a href="/mots-cles?keyword=<?= $keyword ?>">
                                <?= $keyword ?>
                            </a>
                        </li>
                        <?php endforeach ?>
                    </ul>
                </div>

                <div id="date">
                    <h3>Date de publication</h3>
                    <p><?= $page->created()->toDate('d/m/Y') ?>
                    </p>
                </div>


            </div>

            <div id="abstract">
                <h2>Résumé</h2>
                <?= $page->summary() ?>

            </div>

            <div id="citation">
                <h2>Citation</h2>
                <p><?= $page->textAuthors()->toPages()->listAll() ?>, « <?= $page->title() ?> »,
                    <em>Ébullitions</em> [en ligne], publié le <?= $page->created()->toDate('d/m/Y') ?>. DOI: <?= $page->DOI() ?>
                </p>

            </div>
        </section>

        <section id="article-content">
            <?= $page->body()->toBlocks() ?>
        </section>
        <section id="article-biblio">
            <h2>Bibliographie</h2>
            <?= $page->bibliography() ?>
        </section>

        <footer class="footer-content">
            <p>Modifié le
                <?= $page->modified('d/m/Y') ?>
            </p>
            <p class="doi">DOI: <?= $page->DOI() ?></p>
        </footer>

    </section>

    <section class="banner" id="banner-bd">
        <div id="planches-bd">
            <?php foreach($page->bd()->toFiles() as $thumb): ?>
            <figure onclick="openBD()">
                <img 
                    src="<?= $thumb->url() ?>"
                    srcset="<?= $thumb->srcset([
                        '200w'  => ['width' => 400, 'format' => 'webp'],
                        '400w'  => ['width' => 800, 'format' => 'webp'],
                    ]) ?>"
                    sizes="(max-width: 600px) 90vw, 20vw"
                    width="<?= $thumb->resize(600)->width() ?>"
                    height="<?= $thumb->resize(600)->height() ?>"
                    alt="<?= $thumb->alt() ?>"
                    loading="lazy"
                >
            </figure>
            <?php endforeach ?>
        </div>
    </section>

</main>

<aside id="full-bd">
    <header onclick="closeBD()">
        <p class="title-article"> Se comprendre sans parler la même langue ?</p>
        <button onclick="closeBD()">✕</button>
    </header>
    <div class="swiper">
        <div class="swiper-wrapper">
            <?php foreach($page->bd()->toFiles() as $image): ?>
            <div class="swiper-slide">
                <div class="swiper-zoom-container">
                    <img 
                    src="<?= $image->url() ?>"
                    srcset="<?= $image->srcset('webp') ?>"
                    sizes="(max-width: 600px) 90vw, 20vw"
                    width="<?= $image->resize(1200)->width() ?>"
                    height="<?= $image->resize(1200)->height() ?>"
                    alt="<?= $image->alt() ?>"
                    loading="lazy"
                >
                </div>
            </div>
            <?php endforeach ?>
        </div>
        <button class="swiper-button-prev button-prev">←</button>
        <button class="swiper-button-next button-next">→</button>
    </div>
</aside>
<?php snippet('footer') ?>