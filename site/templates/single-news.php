<?php snippet('header', [
    'layout'    => 'no-banner',
    'type'      => 'actu',
    'page-type' => 'actualites'
]) ?>


<?php snippet('nav') ?>

<main>

    <section id="main-content">

        <header class="header-content" id="actu-header">
            <p class="category"><?= $page->category() ?></p>
            <h1><?= $page->title() ?></h1>
            <p class="h1-small"><?= $page->subtitle() ?></p>
            <p class="publication-date">Publié le <?= $page->created()->toDate('d/m/Y') ?></p>
        </header>

  
        <section id="actu-content">

            <div class="chapo">
                <p><?= $page->chapo() ?></p>
            </div>

            <?= $page->body()->toBlocks() ?>
        </section>

        <footer class="footer-content">
            <p>Modifié le <?= $page->modified('d/m/Y') ?></p>
        </footer>

    </section>

</main>
