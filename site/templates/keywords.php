<?php snippet('header', [
    'layout'   => 'banner',
    'type'     => 'site',
    'pageType' => 'keywords'
]) ?>

<?php snippet('nav') ?>

<?php
    $items = $site->index()->filterBy('template', 'in', ['article', 'thematic']);
    $keywords = [];
    foreach ($items as $item) {
        foreach ($item->keywords()->split() as $keyword) {
            if (array_key_exists($keyword, $keywords)) {
                $keywords[$keyword]++;
            } else {
                $keywords[$keyword] = 1;
            }
        }
    }    
?>

<main
    x-data='{
        selectedKeywords: new Set(),
        keywordsCount: <?= json_encode($keywords) ?>,
        count: {
            thematics: document.querySelectorAll(".card[data-type=\"dossier\"]:not(.hidden)").length,
            articles: document.querySelectorAll(".card[data-type=\"article\"]:not(.hidden)").length
        },
        toggleKeyword(keyword) {
            if (this.selectedKeywords.has(keyword)) {
                this.selectedKeywords.delete(keyword)
            } else {
                this.selectedKeywords.add(keyword)
            }

            setTimeout(() => {
                this.updateCount()
            }, 50)

        },
        get isNotEmpty() {
            return this.selectedKeywords.size > 0
        },
        updateCount() {
            this.count.articles = document.querySelectorAll(".article-wrapper:not(.hidden) .card[data-type=\"article\"]").length
            this.count.thematics = document.querySelectorAll(".article-wrapper:not(.hidden) .card[data-type=\"dossier\"]:not(.hidden .card)").length
        },
        itemsPerKeyword(keyword) {
            return this.keywordsCount[keyword];
        },
        init() {
            const query = "<?= get('keyword') ?>";
            if (query.length > 0) {
                this.selectedKeywords.add(query)
            }
        }
    }'
    x-init="
        window.addEventListener('toggleKeyword', (event) => {
            toggleKeyword(event.explicitOriginalTarget.textContent)
        })
        init()
    "
>

    <section id="main-content">

        <header class="header-list">
            <div class="keyword-selected">
                <template x-for="keyword in Array.from(selectedKeywords)">
                    <p x-text="keyword"></p>
                </template>
                <button class="close" :class="isNotEmpty ? '': 'hidden'" @click="selectedKeywords.clear(); setTimeout(() => {
                updateCount()
            }, 50)"><span>✕</span></button>
            </div>
            <ul class="total-cards">
                <li x-text="count.thematics + ' dossier' + (count.thematics > 1 ? 's' : '')"></li>
                <li x-text="count.articles + ' article' + (count.articles > 1 ? 's' : '')"></li>
            </ul> 
            </header>
        </header>
    
        <?php foreach($items as $item): ?>
        <div class="article-wrapper" :class="isVisible ? '': 'hidden'" x-data='{
            keywords: <?= json_encode($item->keywords()->split()) ?>,
            get isVisible() {
                const isKeywordsMatching = Array.from(this.selectedKeywords).every(keyword => this.keywords.includes(keyword));
                return isKeywordsMatching || this.selectedKeywords.size === 0;
            }
        }'>
            <?php snippet('card--list', [
                'card' => $item,
                'type' => $item->template() == 'thematic' ? 'dossier' : 'article'
            ]) ?>
        </div>
        <?php endforeach ?>

    </section>

    <section class="banner banner-keywords">
    <h1>Mots-clés</h1>

    <?php foreach($site->keywords()->split() as $keyword): ?>
        <button class="keyword" :class="selectedKeywords.has('<?= $keyword ?>') ? 'selected': ''" @click="toggleKeyword('<?= $keyword ?>')"><?= $keyword ?><span class="nbr" x-text="itemsPerKeyword('<?= $keyword ?>')"></span></button>
    <?php endforeach ?>
    </section>
</main>
<?php snippet('footer') ?>