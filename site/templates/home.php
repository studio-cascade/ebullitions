<?php snippet('header', [
    'layout' => 'home',
    'type'   => 'home'
]); ?>
<?php snippet('nav'); ?>


<main>
    <div id="main-home">

    <div id="logo-home">
        <?= svg('assets/images/logo.svg') ?>
    </div>

    <div class="home-infos">
        
        <div id="uppa"><?= svg('assets/images/uppa.svg') ?></div>
        <div id="puppa"><?= svg('assets/images/puppa.svg') ?></div>

        <!-- <p class="universite">Université de Pau et des Pays de l’Adour</p> -->
        <button><a href="<?= page('a-propos')->url() ?>">À propos</a></button>
    </div>

    <section id="section-current-num" data-type="numero" class="card-block">
        <?php $lastIssue = $site->find('numeros')->children()->last(); ?>
        <p class="type">Dernier numéro</p>
        <div class="content">
            <figure>
                <a href="<?= $lastIssue->url(); ?>" title="Voir le numéro">
                    <img src="<?= $lastIssue->cover()->toFile()->url(); ?>" alt="<?= $lastIssue->cover()->toFile()->alt() ?>">
                </a>
            </figure>
            <div class="infos">
                <p class="numero">
                    <?php if ($lastIssue->status() == 'listed'): ?>
                        Numéro <?= $lastIssue->num() - 1 ?>
                    <?php else: ?>
                        <span>Hors-série</span>
                    <?php endif ?>
                </p>
                <p class="articles">
                    <?= $lastIssue->articles()->toPages()->count(); ?>
                    articles
                </p>
            </div>
            <h1>
                <a href="<?= $lastIssue->url(); ?>">
                    <?= $lastIssue->title(); ?>
                    <?php if ($lastIssue->subtitle()->isNotEmpty()): ?>
                        <span class="sub-title"><?= $lastIssue->subtitle() ?></span> 
                    <?php endif ?>
                </a>
            </h1>

            <div class="list-articles">
                <p>Sélection d’articles</p>
                <ul>
                    <?php foreach($lastIssue->articles()->toPages()->limit(3) as $item): ?>
                        <li>
                            <a href="<?= $item->url() ?>">
                                <?= $item->title() ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>

        </div>

        <div class="group-button">
            <button class="button-outline" tabindex="-1">
                <a href="<?= $lastIssue->url(); ?>">
                    Lire en ligne
                </a>
            </button>
            <button class="button-full" tabindex="-1"><a href="#">Acheter</a></button>
        </div>
    </section>

    <?php snippet('card--article-cover', ['article' => $page->featuredArticle()->toPage()]) ?>

    <!-- <section id="section-keywords" class="card-block">
        <p class="type">Mots-clés</p>
        <ul>
            <li><a href="#">plurilinguisme<span class="nbr">13</span></a></li>
            <li><a href="#"> laine<span class="nbr">3</span></a></li>
            <li><a href="#"> intercompréhension<span class="nbr">8</span></a></li>
            <li><a href="#"> apprentissage des langues<span class="nbr">8</span></a></li>
            <li><a href="#"> langues romanes<span class="nbr">7</span></a></li>
            <li><a href="#"> bio-inspirations<span class="nbr">6</span></a></li>
            <li><a href="#"> biomimétisme marin<span class="nbr">6</span></a></li>
            <li><a href="#"> polymères naturels<span class="nbr">4</span></a></li>
            <li><a href="#"> biomatériaux<span class="nbr">4</span></a></li>
        </ul>
        <div class="group-button">
            <button class="button-full" tabindex="-1"><a href="pages/keywords.html">Tous les mots-clés</a></button>
        </div>
    </section> -->

    <?php foreach($page->cards()->toPages() as $card): ?>
        <?php if ($card->template() == "single-news"): ?>
            <?php snippet('card--news', ['news' => $card]); ?>
        <?php else: ?>
            <?php snippet('card', ['card' => $card]) ?>
        <?php endif ?>
    <?php endforeach ?>

</div>

<div id="logos-phone">
    <div id="uppa-phone"><?= svg('assets/images/uppa.svg') ?></div>
    <div id="puppa-phone"><?= svg('assets/images/puppa.svg') ?></div>
</div>

</main>
<?php snippet('footer') ?>