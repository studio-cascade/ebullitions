<?php snippet('header', [
    'layout'   => 'group',
    'type'     => 'numero',
    'pageType' => 'page-numero'
]) ?>

<?php snippet('nav') ?>
<?php snippet('bubbles') ?>

<main>
    <?php snippet('banner--issue') ?>
    <div class="banner-phone">
        <a class="banner-link-phone" href="#">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 500 500"><g><g id="sv-voir-bd"><path d="M152.2,393.4c-2.5,0-4.9-.9-6.9-2.5-3-2.6-4.4-6.6-3.4-10.5h0c1.3-5.6,2.6-11,3.9-16.2,3-12.3,5.8-23.9,8.5-35.6,2.1-9,2.4-13.8,1.2-16.7-1.2-2.9-5.1-6.2-13-11-12.8-7.8-23.4-16.6-32.4-27-28.8-33.4-27.2-83.2,3.6-115.9,23.5-24.9,53.8-40.4,92.7-47.3,30.7-5.5,59.5-5.8,85.5-.9,28.8,5.4,55.3,17.3,78.9,35.4,25.5,19.5,39.7,45.1,39.9,71.9.2,27-13.7,52.9-39.2,72.9-31.5,24.8-70.2,37.8-114.8,38.7-13.9.3-21.4,3.9-28.6,13.7-18.8,25.8-45.7,39.5-71.9,50.4-1.3.5-2.7.8-4.1.8ZM161.4,385.1h0s0,0,0,0ZM253.4,126.2c-13.8,0-28.3,1.4-43.5,4.1-34.5,6.2-61.2,19.7-81.6,41.4-23.8,25.2-25,63.5-3,89.1,7.6,8.8,16.6,16.3,27.6,23,22.3,13.5,26.8,24.1,20.9,49.3-2.6,11-5.2,21.9-8,33.3,17.5-8.6,34.2-19.5,46.1-35.9,10.9-14.9,24.1-21.5,44.3-21.9,40.8-.8,74.4-12.1,102.9-34.4,20.5-16.1,31.7-36.4,31.5-57.1-.1-20.5-11.5-40.5-32-56.2-30.4-23.2-65.1-34.7-105.3-34.7Z"/><path d="M500,499.7H0V0h500v499.7ZM19.3,480.4h461.5V19.2H19.3v461.2Z"/></g></g></svg>
            <p>Acheter la version papier</p>
        </a>
    </div>

    <section id="main-content">
        <header class="header-content">
            <p class="category"><?= e($page->status() == 'listed', 'Numéro ' . $page->num() - 1, 'Hors-séries') ?></p>
            <p class="infos"><span><?= $page->articles()->toPages()->count() ?> articles</span></p>
         

            <h1>
                <?= $page->title() ?>
                <?php if ($page->subtitle()->isNotEmpty()): ?>
                    <span class="sub-title"><?= $page->subtitle() ?></span> 
                <?php endif ?>
            </h1>
        </header>

        <section id="edito">
            <input type="checkbox" id="toggle-edito" name="toggle-edito" checked>

            <div id="text-edito">
                <div class="content">

                    <?= $page->edito() ?>

                    <div class="authors-edito section-infos">
                        <?php foreach($page->editoAuthors()->toPages() as $author): ?>
                        <div class="author">
                            <p>
                                <a href="">
                                    <?= $author->title() ?>
                                </a>
                            </p>
                            <p class="author-description">
                                <?= $author->description() ?>
                            </p>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <div class="group-button">
                    <label class="button-full" id="read-edito" for="toggle-edito">Lire l’édito</label>
                    <label class="button-full" id="close-edito" for="toggle-edito">Fermer l’édito</label>
                </div>
            </div>



        </section>

        <section id="list-articles">
        <?php foreach($page->articles()->toPages() as $article): ?>
            <?php snippet('card', ['card' => $article]) ?>
        <?php endforeach ?>
        </section>
    </section>
</main>
<?php snippet('footer') ?>