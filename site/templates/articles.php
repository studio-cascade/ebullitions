<?php snippet('header', [
    'layout'   => 'full',
    'type'     => 'site',
    'pageType' => 'list-articles'
]) ?>




<?php snippet('nav') ?>

<main x-data="{
    search: '',
    resultsCount: document.querySelectorAll('.article-wrapper:not(.hidden)').length,
    get isNoSearch() {
      const isNoSearch = this.search.length === 0
      return isNoSearch
    },
    countResults() {
      setTimeout(() => {
        this.resultsCount = document.querySelectorAll('.article-wrapper:not(.hidden)').length;
      }, 50)
    }
  }">

    <section id="main-content">
        <header class="header-content">
                <h1>Tous les articles</h1>
        </header>
        
        <header class="header-list">
            
            <form class="form-search">
                <button>
                    <?= svg('assets/images/icons/search.svg') ?>
                </button>
                <input @input="countResults()" type="search" id="articles-search" name="articles-search"
                    placeholder="Rechercher" x-model="search" @keydown.enter.prevent />
            </form>
            <ul class="total-cards">
                <li x-text="resultsCount < 2 ? resultsCount + ' article' : resultsCount + ' articles'"></li>
            </ul>
        </header>

        

        <?php foreach($page->children() as $article): ?>
        <div class="article-wrapper" :class="isVisible ? '': 'hidden'" x-data="{
            title: '<?= Str::replace($article->title(), '\'', '') ?>',
            textAuthors: '<?= $article->textAuthors()->toPages()->listAll('string') ?>',
            illustrationsAuthors: '<?= $article->illustrationsAuthors()->toPages()->listAll('string') ?>',
            tags: '<?= $article->keywords() ?>',
            get isVisible() {
                const slugSearch = slugify(search);
                const isTitleMatching = slugify(this.title).includes(slugSearch);
                const isTextAuthorsMatching = slugify(this.textAuthors).includes(slugSearch);
                const isIllustrationsAuthorsMatching = slugify(this.illustrationsAuthors).includes(slugSearch);
                const isTagsMatching = slugify(this.tags).includes(slugSearch);

                return isNoSearch || (search.length > 0 && (isTitleMatching || isTextAuthorsMatching || isIllustrationsAuthorsMatching || isTagsMatching));
            }
        }">
            <?php snippet('card--article-cover', ['article' => $article]) ?>
        </div>
        <?php endforeach ?>
    </section>

    

</main>
<?php snippet('footer') ?>