<?php snippet('header', [
    'layout'   => 'group-list',
    'type'     => 'dossier',
    'pageType' => 'list-dossiers'
]) ?>

<?php snippet('nav') ?>

<main>
    
    <!-- <h1 class="title-phone">Dossiers thématiques</h1> -->

    <div class="container-banners"
        style="--group-nbr: <?= $page->children()->count() ?>;">
    
        <?php foreach($page->children() as $thematic): ?>
        <?php snippet('banner--thematic', ['thematic' => $thematic]) ?>
        <?php endforeach ?>
    </div>
</main>

<?php snippet('footer') ?>